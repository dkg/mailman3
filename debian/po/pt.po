# Portuguese translation for mailman3's debconf messages
# Copyright (C) 2017 Rui Branco <ruipb@debianpt.org>
# This file is distributed under the same license as the mailman3 package.
#
# Rui Branco <ruipb@debianpt.org>, 2017, 2018.
msgid ""
msgstr ""
"Project-Id-Version: mailman3 3.1.1-9\n"
"Report-Msgid-Bugs-To: mailman3@packages.debian.org\n"
"POT-Creation-Date: 2018-03-15 10:57+0100\n"
"PO-Revision-Date: 2018-05-08 14:10+0000\n"
"Last-Translator: Rui Branco - DebianPT <ruipb@debianpt.org>\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Language: Portuguese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Add the HyperKitty configuration to mailman.cfg?"
msgstr "Adicionar a configuração HyperKitty ao mailman.cfg?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Mailman3 needs additional configuration in mailman.cfg in order to send "
"messages to the HyperKitty archiver. This configuration can be added "
"automatically now."
msgstr ""
"O mailman3 requer configuração adicional em mailman.cfg de modo a enviar "
"mensagens para o arquivador HyperKitty. Esta configuração pode ser "
"adicionada automaticamente agora."

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"The content of /usr/share/mailman3/mailman_cfg_hyperkitty_snippet.cfg will "
"be added to /etc/mailman3/mailman.cfg."
msgstr ""
"O conteúdo de /usr/share/mailman3/mailman_cfg_hyperkitty_snippet.cfg será "
"adicionado ao /etc/mailman3/mailman.cfg."

#. Type: error
#. Description
#: ../templates:2001
msgid "The service for mailman3 failed!"
msgstr "O serviço para o mailman3 falhou!"

#. Type: error
#. Description
#: ../templates:2001
msgid ""
"The mailman3 service didn't start correctly. This is probably because you "
"didn't configure the database appropriately. The service won't work until "
"you do so."
msgstr ""
"O serviço mailman3 não arrancou correctamente. Provavelmente porque não "
"configurou a base de dados apropriadamente. O serviço não arrancará até que "
"configure correctamente."

#. Type: error
#. Description
#: ../templates:2001
msgid ""
"If you actually DID install the database appropriately, please report the "
"bug to the maintainers of the mailman3 package."
msgstr ""
"Se efectivamente instalou a base de dados correctamente por favor reporte o "
"erro aos 'maintainers' do pacote mailman3."
